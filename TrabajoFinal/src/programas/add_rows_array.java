package programas;

public class add_rows_array {
    public static void main(String[] args) {
        String [][] array1 = {{"a","b","c"},{"d","e","f"}};
        int columnas = 10;
        String[][] array2 = add_rows(array1, columnas);
        System.out.println( " filas y cols " + array2.length + " " + array2[0].length);
        System.out.println(" columna 13 " + array2[0][12]);

    }
    static String [][] add_rows ( String[][] origen, int rows) {
        String [][] destino = new String[origen.length][(origen[0].length + rows)];
        for (int i=0; i < origen.length; i++) {
            destino[i] = new String[origen[i].length+rows];
            System.arraycopy(origen[i],0,destino[i],0,origen[i].length);
        }
    return destino;
    }
}
