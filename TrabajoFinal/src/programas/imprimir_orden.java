package programas;

public class imprimir_orden {
    public static void main(String[] args) {
        //String lista_vacia = "|aaa|bbbbbbbbbb|cccccccccccccccccccccccccccccccccccccccccccccccccccc|    |            |            |"
        String lista_vacia = "|aaa|bbbbbbbbbb|cccccccccccccccccccccccccccccccccccccccccccccccccccc|dddd|eeeeeeeeeeee|gggggggggggg|ffffffffffff|";
        String h_item = String.format("%1$3s", "IT.");
        lista_vacia = lista_vacia.replaceAll("aaa", h_item);
        String h_codigo = String.format("%1$-9s", "CODIGO");
        lista_vacia = lista_vacia.replaceAll("bbbbbbbbbb", h_codigo);
        String h_texto = String.format("%1$-50s", "DESCRIPCION DEL BIEN");
        lista_vacia = lista_vacia.replaceAll("cccccccccccccccccccccccccccccccccccccccccccccccccccc", h_texto);
        String h_unidad = String.format("%1$-4s", "UOM");
        lista_vacia = lista_vacia.replaceAll("dddd", h_unidad);
        String h_cantidad = String.format("%1$12s", "CANTIDAD");
        lista_vacia = lista_vacia.replaceAll("eeeeeeeeeeee", h_cantidad);
        String h_punit = String.format("%1$12s", "PRECIO UNIT");
        lista_vacia = lista_vacia.replaceAll("gggggggggggg", h_punit);
        String h_subtotal = String.format("%1$12s", "SUBTOTAL");
        lista_vacia = lista_vacia.replaceAll("ffffffffffff", h_subtotal);
        System.out.println(lista_vacia);
        System.out.println("-------------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < 5; i++) {
            lista_vacia = "|aaa|bbbbbbbbbb|cccccccccccccccccccccccccccccccccccccccccccccccccccc|dddd|eeeeeeeeeeee|gggggggggggg|ffffffffffff|";
            String item = String.format("%1$3s", "1");
            lista_vacia = lista_vacia.replaceAll("aaa", item);
            String codigo = String.format("%1$-9s", "12345678");
            lista_vacia = lista_vacia.replaceAll("bbbbbbbbbb", codigo);
            String texto = String.format("%1$-50s", "qwertyuiopasdfghjk wertyui");
            lista_vacia = lista_vacia.replaceAll("cccccccccccccccccccccccccccccccccccccccccccccccccccc", texto);
            String unidad = String.format("%1$-4s", "kls");
            lista_vacia = lista_vacia.replaceAll("dddd", unidad);
            String cantidad = String.format("%1$12s", "9876543210");
            lista_vacia = lista_vacia.replaceAll("eeeeeeeeeeee", cantidad);
            String punit = String.format("%1$12s", "122.99");
            lista_vacia = lista_vacia.replaceAll("gggggggggggg", punit);
            String subtotal = String.format("%1$12s", "1234567890");
            lista_vacia = lista_vacia.replaceAll("ffffffffffff", subtotal);
            System.out.println(lista_vacia);

        }
        System.out.println("-------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                            el total de la oc es : ");
    }
}