// codigo RRS
package programas;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.io.File;
import java.nio.file.Path;
import java.util.Scanner;


public class test_matriz_oc {
    public static void main(String[] args) throws IOException {
        String ruta_archivo1;
        String ruta_archivo2;
        String ruta_archivo3;
            ruta_archivo1 = "D:/precios_2021.csv";
            ruta_archivo2 = "D:/requerimientos_2021.csv";
            ruta_archivo3 = "D:/catalogo.csv";
        String[][] tabla_adjudicacion = cargar_array_csv(ruta_archivo1);
        String[][] tabla_compras = cargar_array_csv(ruta_archivo2);
        String[][] tabla_materiales = cargar_array_csv(ruta_archivo3);

                    //04.prueba de las matrices de precios y requerimientos
                    /*System.out.println("prueba " + tabla_compras[4208][0]);
                    System.out.println("prueba " + tabla_adjudicacion[1][1]);
                    System.out.println("prueba " + tabla_compras[4208][7]);
                    System.out.println("prueba " + tabla_materiales[36348][11]);*/

        System.out.println("matriz filas " + (tabla_compras.length));
        System.out.println("matriz columnas " + (tabla_compras[0].length));
        //definir tabla ordenes mismas filas  y 3 columnas adicionales - p.unit , subtotal , categoria
        String [][] tabla_ordenes = new String[tabla_compras.length][(tabla_compras[0].length + 3)];
        //test todos los valores son null
        System.out.println("matriz oc " + tabla_ordenes[14895][11]);
        System.out.println("matriz filas " + (tabla_ordenes.length));
        System.out.println("matriz columnas " + (tabla_ordenes[0].length));
        // copia los valores del array original
        //Collections.addAll(tabla_ordenes,tabla_compras);
        //tabla_ordenes = Arrays.copyOfRange(tabla_compras,0,tabla_compras.length);
        //System.arraycopy(tabla_compras,0,tabla_ordenes,0,tabla_ordenes.length);

        for (int i=0; i < tabla_compras.length; i++) {
            tabla_ordenes[i] = new String[tabla_compras[i].length+3];
            System.arraycopy(tabla_compras[i],0,tabla_ordenes[i],0,tabla_compras[i].length);
        }

        System.out.println("matriz filas " + (tabla_adjudicacion.length));
        System.out.println("matriz columnas " + (tabla_adjudicacion[0].length));
        System.out.println("matriz oc1 " + tabla_adjudicacion[0][1]);
        System.out.println("matriz oc2 " + tabla_adjudicacion[4208][1]);
        System.out.println("matriz oc1 " + tabla_ordenes[14895][9]);

                    // tomar tabla adjudicados  e incluir en nuevo array con categorias del catalogo
                    // calcular la cantidad total de provedores
                    // calcular la cantidad de categorias para cada proveedor ejem prov A, 10 categorias

                    // tomar array de compras y migrar a otro arreglo con 3 columnas conteniendo null
                    // calcular las colmnad adicionales ( precio unitario, subtotal, numero de orden de compra)

                    // generación de estadisticas : cant total de ordenes, monto total a comprar
                    // por DZ - almacen, por categoria, por proveedor
                    // desea imprir alguna orden (bucle para imprimir hasta que el usuario termine el proceso)
                    // 3 metodos: a) genera ordenes masivas b) genera estadisticas c) bucle de impresion
                }


    // metodo para almacenar CSV en array
    static String[][] cargar_array_csv(String archivo) throws IOException {
        Path ruta = Paths.get(archivo);
        List<String> lines = Files.readAllLines(ruta);
        String[] header = lines.get(0).split(",");
        String[][] datos = new String[lines.size()][header.length];
        int i;
        for (i = 0; i < lines.size(); i++) {
            datos[i] = lines.get(i).split(",");
        }
        return datos;
    }
}