package programas;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.nio.file.Path;
//import java.util.Arrays
//import java.io.File


public class codigo_principal {
    public static void main(String[] args) throws IOException {
        // 2 metodos principales : a) genera ordenes masivas b) impresion de orden

        // se debe convertir los archivo de datos en arreglos
        // scanner para ingresar ruta de archivos
        //01.Declaracion de Variables

        Scanner sc = new Scanner(System.in);
        String ruta_archivo1;
        String ruta_archivo2;
        String ruta_archivo3;
        int correlativo;
        String procesar = "";
        int impresion;

        //02. seleccion de archivos - lectura de datos y almacenamiento en array

        while (!procesar.equals("T")) {
            // ingresar ruta de archivo articulos adjudicados
            System.out.println("Ingresar ruta del archivo de proveedores y precios adjudicados: ");
            ruta_archivo1 = sc.nextLine();
            // usar metodo para almacenar csv1
            String[][] tabla_adjudicacion = cargar_array_csv(ruta_archivo1);
            // ingresar ruta de archivo de necesidad de compras
            System.out.println("Ingresar ruta del archivo de requerimientos de compra: ");
            ruta_archivo2 = sc.nextLine();
            // usar metodo para almacenar csv2
            String[][] tabla_compras = cargar_array_csv(ruta_archivo2);
            // ingresar ruta de archivo de catalogo de bienes
            System.out.println("Ingresar ruta del archivo de materiales: ");
            ruta_archivo3 = sc.nextLine();
            // usar metodo para almacenar csv3
            String[][] tabla_materiales = cargar_array_csv(ruta_archivo3);
            // confirmar correr proceso de generación
            System.out.println("Procesar ordenes de compra: S (procesar) C (corregir archivos) ó T (para Terminar)");
            procesar = sc.nextLine();
            while (!procesar.equals("s") & !procesar.equals("c") & !procesar.equals("t") & !procesar.equals("S") & !procesar.equals("C") & !procesar.equals("T")) {
                System.out.println("solo debe ingresar S (procesar) C (corregir archivos) ó T (para Terminar)");
                System.out.println("-------------------------------------------------------------------------");
                procesar = sc.nextLine();
            }
            switch (procesar) {
                case "T", "t" -> {
                    System.out.println("se interrumpio la ejecución ");
                    System.out.println(":'(");
                    procesar = "T";
                }
                case "C", "c" -> {
                    System.out.println("ingrese parametros nuevamente....");
                    System.out.println(":'(");
                    procesar = "";
                }
                case "S", "s" -> {
                    // ultima orden generada
                    System.out.println("Ingrese la ultima orden generada: ");
                    correlativo = sc.nextInt();
                    System.out.println("Generando OC ...");
                    // tomar tabla adjudicados  e incluir en nuevo fila (1) en blanco para incl categorias del catalogo
                    String[][] temp1 = add_rows(tabla_adjudicacion, 1);
                    // agregar información de categorias del catalogo a tabla_precios
                    String[][] tabla_precios = vlook_up(tabla_materiales, 9, 6, temp1, 2, 4);
                    // tomar tabla compras  e incluir en nuevo fila (8) en blanco para incl calculos de OC: descrip_bien,unid_med,punit,subtotal,posicion,num_oc
                    String[][] temp2 = add_rows(tabla_compras, 8);
                    // agregar información de p_unit adjudicados a tabla compras
                    String[][] temp3 = vlook_up(tabla_materiales, 9, 11, temp2, 7, 9); //descrip
                    String[][] temp4 = vlook_up(tabla_materiales, 9, 10, temp3, 7, 10); //unid_medida
                    String[][] temp5 = vlook_up(tabla_precios, 2, 3, temp4, 7, 11); //p_unit
                    String[][] temp6 = vlook_up(tabla_materiales, 9, 6, temp5, 7, 12); //categoria
                    String[][] tabla_ordenes = vlook_up(tabla_precios, 2, 1, temp6, 7, 13); //proveedor

                    //vector proveedores categorias almacenes
                    String[] proveedores = count_distinct(tabla_precios, 1);
                    String[] categorias = count_distinct(tabla_precios, 4);
                    String[] almacenes = count_distinct(tabla_ordenes, 1); // tambien puede ser la tabla_compras

                    // Informar tamaño de vectores de n elementos para los for que generan OC
                    System.out.println("Procesado: ");
                    System.out.println("Total Almacenes " + almacenes.length);
                    System.out.println("Total Categorias " + categorias.length);
                    System.out.println("Total Proveedores " + proveedores.length);

                    // metodo para calculos en tabla_ordenes y guardar resumen lista_ordenes
                    ArrayList<ArrayList<String>> lista_ordenes = oc_generadas(tabla_ordenes, proveedores, categorias, almacenes, correlativo);

                    // consulta orden a imprimir y llama metodo
                    System.out.println("....................................");
                    System.out.println("ingrese orden a imprimir: ");
                    impresion = sc.nextInt();
                    System.out.println("....................................");
                    // metodo para imprimir 1 orden
                    String prueba_impresion = print_orden(tabla_ordenes, lista_ordenes, impresion);
                    System.out.println(prueba_impresion);
                    // proceso finalizado
                    System.out.println(" :) ");
                    procesar = "T";

                }
            }

        }
        System.out.println(" proceso finalizado ");
    }

    // metodo para almacenar CSV en array
    static String[][] cargar_array_csv(String archivo) throws IOException {
        Path ruta = Paths.get(archivo);
        List<String> lines = Files.readAllLines(ruta);
        String[] header = lines.get(0).split(",");
        String[][] datos = new String[lines.size()][header.length];
        int i;
        for (i = 0; i < lines.size(); i++) {
            datos[i] = lines.get(i).split(",");
        }
        return datos;
    }

    // metodo para adicionar "n" filas en blanco a array 2d
    static String[][] add_rows(String[][] origen, int rows) {
        String[][] destino = new String[origen.length][(origen[0].length + rows)];
        for (int i = 0; i < origen.length; i++) {
            destino[i] = new String[origen[i].length + rows];
            System.arraycopy(origen[i], 0, destino[i], 0, origen[i].length);
        }
        return destino;
    }

    // metodo para llevar valores de matriz A a matriz B en funcion a una coincidencia previa
    static String[][] vlook_up(String[][] array_origen, int buscadoA, int valorA, String[][] array_destino,
                               int buscadoB, int destinoB) {
        for (int i = 0; i < array_destino.length; i++) {
            for (int j = 0; j < array_origen.length; j++) {
                // se reemplazo por for enhanced para strings
                //for (String[] strings : array_origen) {
                if (array_destino[i][buscadoB].equals(array_origen[j][buscadoA])) {
                    array_destino[i][destinoB] = array_origen[j][valorA];
                }
            }
        }
        return array_destino;
    }

    static String[] count_distinct(String[][] elementos, int indice) {
        Set<String> set = new HashSet<>();
        for (int i = 1; i < elementos.length; i++) { //cero toma las cabeceras
            set.add(elementos[i][indice]);
        }
        //System.out.println( "los unicos " +  set);
        return set.toArray(new String[0]);
    }

    static ArrayList<ArrayList<String>> oc_generadas(String[][] t_ordenes, String[] prov, String[] cat, String[] alm, int last_OC) { //String [][] oc_generadas
        // metodo para generar ordenes
        int num_OC;
        ArrayList<ArrayList<String>> lista_mayor;
        lista_mayor = new ArrayList<>();
        for (int i = 0; i < prov.length; i++) {
            for (int j = 0; j < cat.length; j++) {
                for (int k = 0; k < alm.length; k++) {
                    num_OC = last_OC + 1;
                    int item = 0;
                    double sum_orden = 0.0;
                    String comprador = "";
                    String fecha_entrega = "";
                    String condicion_pago = "";
                    for (int l = 1; l < t_ordenes.length; l++) {
                        if (t_ordenes[l][1].equals(alm[k]) & t_ordenes[l][12].equals(cat[j]) &
                                t_ordenes[l][13] != null && t_ordenes[l][13].equals(prov[i])) {
                            item = item + 1;
                            t_ordenes[l][15] = Integer.toString(item);//compras_item[l]=item+1;
                            double subtotal_linea_oc = Double.parseDouble(t_ordenes[l][8]) * Double.parseDouble(t_ordenes[l][11]);
                            t_ordenes[l][14] = String.valueOf(subtotal_linea_oc); //subtotlal[l]=punit[l]*cantidad[l];
                            t_ordenes[l][16] = Integer.toString(num_OC); //compras_oc[l]=num_OC;
                            last_OC = num_OC;
                            sum_orden = sum_orden + subtotal_linea_oc;
                            if (item == 1) {
                                comprador = t_ordenes[l][3];
                                fecha_entrega = t_ordenes[l][4];
                                condicion_pago = t_ordenes[l][5];
                            }
                            //prueba impresion de insercion en tabla compras
                            //System.out.println(" datos insertados " + tabla_ordenes[l][13] + " "+ tabla_ordenes[l][15] + " " + tabla_ordenes[l][14] + " " + tabla_ordenes[l][16]);
                        }
                    }
                    // aqui se guarda la orden y su total en un array independiente prov&cat&alm&orden&total
                    if (item != 0) {
                        // guardar valores array list empezar conlista_interior.Clear();
                        ArrayList<String> lista_menor = new ArrayList<>();
                        lista_menor.add(prov[i]);
                        lista_menor.add(cat[j]);
                        lista_menor.add(alm[k]);
                        lista_menor.add(comprador);
                        lista_menor.add(fecha_entrega);
                        lista_menor.add(condicion_pago);
                        lista_menor.add(Integer.toString(last_OC));
                        lista_menor.add(Integer.toString(item));
                        lista_menor.add(String.valueOf(sum_orden));
                        lista_mayor.add(lista_menor);
                        //System.out.println(" resumen de datos " + proveedores[i] + " iniciales comprador: " + comprador + " categoria: " + categorias[j] + " destino: " + almacenes[k] + " orden: " + last_OC + " Entregar el: " + fecha_entrega + " items: " + item + " monto: " + (Math.round(sum_orden * 100.0) / 100.0));
                        System.out.println(" resumen oc " + prov[i] + " comprador: " + comprador + " rubro: " + cat[j] + " destino: " + alm[k] + " orden: " + last_OC + " entrega: " + fecha_entrega + " items: " + item + " monto: " + (Math.round(sum_orden * 100.0) / 100.0));
                    }
                }
            }
        }
        // prueba de datos lista_mayor
        // System.out.println(" lista mayor ")
        // System.out.println(lista_mayor)
        return lista_mayor;
    }

    static String print_orden(String[][] cuerpo, ArrayList<ArrayList<String>> resumenes, int num_oc_impr) {
        // lineas de cabecera para impresion de orden
        String lista_vacia = "|aaa|bbbbbbbbbb|cccccccccccccccccccccccccccccccccccccccccccccccccccc|dddd|eeeeeeeeeeee|gggggggggggg|ffffffffffff|";
        String h_item = String.format("%1$3s", "IT.");
        lista_vacia = lista_vacia.replaceAll("aaa", h_item);
        String h_codigo = String.format("%1$-9s", "CODIGO");
        lista_vacia = lista_vacia.replaceAll("bbbbbbbbbb", h_codigo);
        String h_texto = String.format("%1$-50s", "DESCRIPCION DEL BIEN");
        lista_vacia = lista_vacia.replaceAll("cccccccccccccccccccccccccccccccccccccccccccccccccccc", h_texto);
        String h_unidad = String.format("%1$-4s", "UOM");
        lista_vacia = lista_vacia.replaceAll("dddd", h_unidad);
        String h_cantidad = String.format("%1$12s", "CANTIDAD");
        lista_vacia = lista_vacia.replaceAll("eeeeeeeeeeee", h_cantidad);
        String h_punit = String.format("%1$12s", "PRECIO UNIT");
        lista_vacia = lista_vacia.replaceAll("gggggggggggg", h_punit);
        String h_subtotal = String.format("%1$12s", "SUBTOTAL");
        String cabecera = lista_vacia.replaceAll("ffffffffffff", h_subtotal);

        //usa el numero a imprimir y busca con un for , luego imprime cabeceras y detalle
        for (int x = 0; x < resumenes.size(); x++) {
            ArrayList<String> lista = resumenes.get(x);
            if (Integer.toString(num_oc_impr).equals(lista.get(6))) {
                System.out.println("ORDEN: " + num_oc_impr);
                System.out.println("PROVEEDOR: " + lista.get(0));
                System.out.println("ALMACEN DESTINO: " + lista.get(2));
                System.out.println("COD COMPRADOR: " + lista.get(3));
                System.out.println("FECHA ENTREGA: " + lista.get(4));
                System.out.println(" ");
                System.out.println(cabecera);
                System.out.println("-------------------------------------------------------------------------------------------------------------");
                // a partir de aqui trae las lineas de la orden ingresada
                for (int i = 0; i < cuerpo.length; i++) {
                    if (Integer.toString(num_oc_impr).equals(cuerpo[i][16])) {
                        //for (int j = 0; j < cuerpo[0].length; j++)
                        lista_vacia = "|aaa|bbbbbbbbbb|cccccccccccccccccccccccccccccccccccccccccccccccccccc|dddd|eeeeeeeeeeee|gggggggggggg|ffffffffffff|";
                        String item = String.format("%1$3s", cuerpo[i][15]);
                        lista_vacia = lista_vacia.replaceAll("aaa", item);
                        String codigo = String.format("%1$-9s", cuerpo[i][7]);
                        lista_vacia = lista_vacia.replaceAll("bbbbbbbbbb", codigo);
                        String texto = String.format("%1$-50s", cuerpo[i][9]);
                        lista_vacia = lista_vacia.replaceAll("cccccccccccccccccccccccccccccccccccccccccccccccccccc", texto);
                        String unidad = String.format("%1$-4s", cuerpo[i][10]);
                        lista_vacia = lista_vacia.replaceAll("dddd", unidad);
                        String cantidad = String.format("%1$12s", Math.round(Double.parseDouble(cuerpo[i][8])*100)/100.0);
                        lista_vacia = lista_vacia.replaceAll("eeeeeeeeeeee", cantidad);
                        String punit = String.format("%1$12s", Math.round(Double.parseDouble(cuerpo[i][11])*100)/100.0);
                        lista_vacia = lista_vacia.replaceAll("gggggggggggg", punit);
                        String subtotal = String.format("%1$12s", Math.round(Double.parseDouble(cuerpo[i][14])*100)/100.0);
                        lista_vacia = lista_vacia.replaceAll("ffffffffffff", subtotal);
                        System.out.println(lista_vacia);
                    }
                    //System.out.println("-------------------------------------------------------------------------------------------------------------")
                    //System.out.println("                                                            el total de la oc es : " + (Math.round(Double.parseDouble(lista.get(8)) * 100) / 100.0))
                }
                System.out.println("-------------------------------------------------------------------------------------------------------------");
                System.out.println("                                                            el total de la oc es : " + (Math.round(Double.parseDouble(lista.get(8)) * 100) / 100.0));
            }
        }
        return "----final----";
    }
}


