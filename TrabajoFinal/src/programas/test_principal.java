package programas;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.io.File;
import java.nio.file.Path;
import java.util.Scanner;


public class test_principal {
    public static void main(String[] args) throws IOException {
        // convertir ambos archivos en arreglos de variable global


        // scanner para ingresar ruta de archivos
        //01.Declaracion de Variables
        Scanner sc = new Scanner(System.in);
        String ruta_archivo1;
        String ruta_archivo2;
        String ruta_archivo3;
        String procesar = "";

        //02. seleccion de archivos - lectura de datos y almacenamiento en array

        while (!procesar.equals("T")) {
            // ingresar ruta de archivo articulos adjudicados
            System.out.println("Ingresar ruta del archivo de proveedores y precios adjudicados: ");
            ruta_archivo1 = sc.nextLine();
            // usar metodo para almacenar csv1
            String[][] tabla_adjudicacion = cargar_array_csv(ruta_archivo1);
            // ingresar ruta de archivo de necesidad de compras
            System.out.println("Ingresar ruta del archivo de requerimientos de compra: ");
            ruta_archivo2 = sc.nextLine();
            // usar metodo para almacenar csv2
            String[][] tabla_compras = cargar_array_csv(ruta_archivo2);
            // ingresar ruta de archivo de catalogo de bienes
            System.out.println("Ingresar ruta del archivo de materiales: ");
            ruta_archivo3 = sc.nextLine();
            // usar metodo para almacenar csv3
            String[][] tabla_materiales = cargar_array_csv(ruta_archivo3);
            // ultima orden generada
            System.out.println("Ingrese la ultima orden generada: ");
            int last_OC = sc.nextInt();
            // confirmar correr proceso de generación
            System.out.println("Procesar ordenes de compra: S (procesar) C (corregir archivos) ó T (para Terminar)");
            procesar = sc.nextLine();
            while (!procesar.equals("s") & !procesar.equals("c") & !procesar.equals("t") & !procesar.equals("S") & !procesar.equals("C") & !procesar.equals("T")) {
                System.out.println("solo debe ingresar S (procesar) C (corregir archivos) ó T (para Terminar)");
                System.out.println("-------------------------------------------------------------------------");
                procesar = sc.nextLine();
            }
            switch (procesar) {
                case "T", "t" -> {
                    System.out.println("se interrumpio la ejecución ");
                    System.out.println(":'(");
                    procesar = "T";
                }
                case "C", "c" -> {
                    System.out.println("ingrese parametros nuevamente....");
                    System.out.println(":'(");
                    procesar = "";
                }
                case "S", "s" -> {

                    // tomar tabla adjudicados  e incluir en nuevo fila (1) en blanco para incl categorias del catalogo
                    String[][] temp1 = add_rows(tabla_adjudicacion, 1);
                    // agregar información de categorias del catalogo a tabla_precios
                    String[][] tabla_precios = vlook_up(tabla_materiales, 9, 6, temp1, 2, 4);
                    // tomar tabla compras  e incluir en nuevo fila (8) en blanco para incl calculos de OC: descrip_bien,unid_med,punit,categoria,proveedor,subtotal,posicion,num_oc
                    String[][] temp2 = add_rows(tabla_compras, 8);
                    // agregar información de p_unit adjudicados a tabla compras
                    String[][] temp3 = vlook_up(tabla_materiales, 9, 11, temp2, 7, 9); //descrip
                    String[][] temp4 = vlook_up(tabla_materiales, 9, 10, temp3, 7, 10); //unid_medida
                    String[][] temp5 = vlook_up(tabla_precios, 2, 3, temp4, 7, 11); //p_unit
                    String[][] temp6 = vlook_up(tabla_materiales, 9, 6, temp5, 7, 12); //categoria
                    String[][] tabla_ordenes = vlook_up(tabla_precios, 2, 1, temp6, 7, 13); //proveedor

                    //vector proveedores categorias almacenes
                    String[] proveedores = count_distinct(tabla_precios, 1);
                    String[] categorias = count_distinct(tabla_precios, 4);
                    String[] almacenes = count_distinct(tabla_ordenes, 1); // tambien puede ser la tabla_compras

                    //System.out.println(" xxx " + tabla_ordenes[1][11]);
                    //System.out.println(" xxx " + tabla_ordenes[1][12]);

                    // metodo para generar ordenes
                    int num_OC;
                    //int last_OC = 10000;
                    ArrayList<ArrayList<String>> lista_mayor;
                    lista_mayor = new ArrayList<>();
                    for (int i = 0; i < proveedores.length; i++) {
                        for (int j = 0; j < categorias.length; j++) {
                            for (int k = 0; k < almacenes.length; k++) {
                                num_OC = last_OC + 1;
                                int item = 0;
                                double sum_orden = 0.0;
                                String comprador = "";
                                String fecha_entrega = "";
                                String condicion_pago = "";
                                for (int l = 1; l < tabla_ordenes.length; l++) {
                                    if (tabla_ordenes[l][1].equals(almacenes[k]) & tabla_ordenes[l][12].equals(categorias[j]) &
                                            tabla_ordenes[l][13] != null && tabla_ordenes[l][13].equals(proveedores[i])) {
                                        item = item + 1;
                                        tabla_ordenes[l][15] = Integer.toString(item);//compras_item[l]=item+1;
                                        double subtotal_linea_oc = Double.parseDouble(tabla_ordenes[l][8]) * Double.parseDouble(tabla_ordenes[l][11]);
                                        tabla_ordenes[l][14] = String.valueOf(subtotal_linea_oc); //subtotlal[l]=punit[l]*cantidad[l];
                                        tabla_ordenes[l][16] = Integer.toString(num_OC); //compras_oc[l]=num_OC;
                                        last_OC = num_OC;
                                        sum_orden = sum_orden + subtotal_linea_oc;
                                        if (item == 1) {
                                            comprador = tabla_ordenes[l][3];
                                            fecha_entrega = tabla_ordenes[l][4];
                                            condicion_pago = tabla_ordenes[l][5];
                                        }
                                        //prueba impresion de insercion en tabla compras
                                        //System.out.println(" datos insertados " + tabla_ordenes[l][13] + " "+ tabla_ordenes[l][15] + " " + tabla_ordenes[l][14] + " " + tabla_ordenes[l][16]);
                                    }
                                }
                                // aqui se guarda la orden y su total en un array independiente prov&cat&alm&orden&total
                                //lista_mayor = new ArrayList<>();
                                if (item != 0) {
                                    // guardar valores array list empezar conlista_interior.Clear();
                                    ArrayList<String> lista_menor = new ArrayList<>();
                                    lista_menor.add(proveedores[i]);
                                    lista_menor.add(categorias[j]);
                                    lista_menor.add(almacenes[k]);
                                    lista_menor.add(comprador);
                                    lista_menor.add(fecha_entrega);
                                    lista_menor.add(condicion_pago);
                                    lista_menor.add(Integer.toString(last_OC));
                                    lista_menor.add(Integer.toString(item));
                                    lista_menor.add(String.valueOf(sum_orden));
                                    lista_mayor.add(lista_menor);
                                    //System.out.println(" resumen de datos " + proveedores[i] + " iniciales comprador: " + comprador + " categoria: " + categorias[j] + " destino: " + almacenes[k] + " orden: " + last_OC + " Entregar el: " + fecha_entrega + " items: " + item + " monto: " + (Math.round(sum_orden * 100.0) / 100.0));
                                    System.out.println(" resumen oc " + proveedores[i] + " comprador: " + comprador + " rubro: " + categorias[j] + " destino: " + almacenes[k] + " orden: " + last_OC + " entrega: " + fecha_entrega + " items: " + item + " monto: " + (Math.round(sum_orden * 100.0) / 100.0));
                                }
                            }
                        }
                    }
                    // prueba collection
                    System.out.println(" lista mayor ");
                    System.out.println(lista_mayor);
                    //

                    // pruebas vectores de n elementos para los for que generan OC
                    System.out.println("almacenes " + almacenes.length);
                    System.out.println("categorias " + categorias.length);
                    System.out.println("cant prov " + proveedores.length);
                    System.out.println("proveedor 1 " + proveedores[0]);


                    // pruebas
                    // puede servir para la impresion de la orden
                    System.out.println("tabla ordenes " + tabla_ordenes[1][7]); //cod
                    System.out.println("tabla ordenes " + tabla_ordenes[1][9]); //descr
                    System.out.println("tabla ordenes " + tabla_ordenes[1][10]); //und
                    System.out.println("tabla ordenes " + tabla_ordenes[1][11]); //punit
                    System.out.println("tabla ordenes " + tabla_ordenes[1][8]); //cant
                    System.out.println("tabla ordenes " + tabla_ordenes[1][13]); //prov
                    System.out.println("tabla ordenes " + tabla_ordenes[1][16]); //oc


                    System.out.println(" precios " + tabla_precios[1][4]); // categoria
                    System.out.println("materiales " + tabla_materiales[0].length);
                    System.out.println("materiales " + tabla_materiales[1][8]);
                    System.out.println("materiales " + tabla_materiales[1][7]);

                    System.out.println("matriz oc " + tabla_ordenes[14895][11]);
                    System.out.println("matriz filas " + (tabla_ordenes.length));
                    System.out.println("matriz columnas " + (tabla_ordenes[0].length));
                    System.out.println("matriz precio " + (temp1.length));

                    System.out.println("matriz filas " + (tabla_ordenes.length));
                    System.out.println("matriz columnas " + (tabla_ordenes[0].length));
                    System.out.println("matriz oc1 " + tabla_ordenes[14895][8]);
                    System.out.println("matriz oc2 " + tabla_ordenes[14895][7]);
                    System.out.println("matriz oc1 " + tabla_ordenes[14895][9]);

                    //04.prueba de las matrices de precios y requerimientos
                    System.out.println("prueba " + tabla_compras[4208][0]);
                    System.out.println("prueba " + tabla_adjudicacion[1][3]);
                    System.out.println("prueba " + tabla_compras[4208][7]);
                    System.out.println("prueba " + tabla_materiales[36348][11]);
                    System.out.println(":)");
                    procesar = "T";

                    // hasta esta linea el codigo esta probado

                    // calcular la cantidad total de provedores
                    // calcular la cantidad de categorias para cada proveedor ejem prov A, 10 categorias

                    // tomar array de compras y migrar a otro arreglo con 3 columnas conteniendo null
                    // calcular las colmnad adicionales ( precio unitario, subtotal, numero de orden de compra)

                    // generación de estadisticas : cant total de ordenes, monto total a comprar
                    // por DZ - almacen, por categoria, por proveedor
                    // desea imprir alguna orden (bucle para imprimir hasta que el usuario termine el proceso)
                    // 3 metodos: a) genera ordenes masivas b) genera estadisticas c) bucle de impresion
                }
            }

        }
        System.out.println(" proceso finalizado ");
    }

    // metodo para almacenar CSV en array
    static String[][] cargar_array_csv(String archivo) throws IOException {
        Path ruta = Paths.get(archivo);
        List<String> lines = Files.readAllLines(ruta);
        String[] header = lines.get(0).split(",");
        String[][] datos = new String[lines.size()][header.length];
        int i;
        for (i = 0; i < lines.size(); i++) {
            datos[i] = lines.get(i).split(",");
        }
        return datos;
    }

    // metodo para adicionar "n" filas en blanco a array 2d
    static String [][] add_rows( String[][] origen, int rows) {
        String [][] destino = new String[origen.length][(origen[0].length + rows)];
        for (int i=0; i < origen.length; i++) {
            destino[i] = new String[origen[i].length+rows];
            System.arraycopy(origen[i],0,destino[i],0,origen[i].length);
        }
        return destino;
    }

    // metodo para llevar valores de matriz A a matriz B en funcion a una coincidencia previa
    static String[][] vlook_up (String[][]array_origen,int buscadoA, int valorA, String[][] array_destino,
                                int buscadoB, int destinoB){
        for (int i = 0; i < array_destino.length; i++) {
            for (int j = 0; j < array_origen.length; j++) {
                // se reemplazo por for enhanced para strings
                //for (String[] strings : array_origen) {
                if (array_destino[i][buscadoB].equals(array_origen[j][buscadoA])) {
                    array_destino[i][destinoB] = array_origen[j][valorA];
                }
            }
        }
        return array_destino;
    }
    static String [] count_distinct (String [][]elementos, int indice) {
        Set<String> set = new HashSet<>();
        for (int i = 1; i < elementos.length; i++) { //cero toma las cabeceras
            set.add(elementos[i][indice]);
        }
        //System.out.println( "los unicos " +  set);
        return set.toArray(new String[0]);
    }
    // static String [][] oc_generadas (String [][] t_compras, String[] prov, String[] cat, String[] alm,int correlativo) {
    //    return oc_generadas
    //}
}
